/********************************************************************************
** Form generated from reading UI file 'configvx3p.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIGVX3P_H
#define UI_CONFIGVX3P_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConfigVX3p
{
public:
    QGridLayout *gridLayout;
    QWidget *widget;
    QGridLayout *gridLayout_7;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *horizontalSpacer_5;
    QWidget *widget_6;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_10;
    QPushButton *butRead;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *butSave;
    QPushButton *butCancel;
    QWidget *widget_2;
    QGridLayout *gridLayout_2;
    QLabel *lblName;
    QLineEdit *editName;
    QLabel *lblId;
    QLineEdit *editId;
    QWidget *widget_3;
    QGridLayout *gridLayout_3;
    QGroupBox *groupSampleRate;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioSampleRate1;
    QRadioButton *radioSampleRate2;
    QRadioButton *radioSampleRate3;
    QRadioButton *radioSampleRate4;
    QGroupBox *groupDuration;
    QVBoxLayout *verticalLayout_2;
    QRadioButton *radioDuration96;
    QRadioButton *radioDuration72;
    QRadioButton *radioDuration48;
    QRadioButton *radioDuration24;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *horizontalSpacer_13;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_6;
    QWidget *widget_4;
    QGridLayout *gridLayout_6;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_4;
    QRadioButton *radioDatePref1;
    QRadioButton *radioDatePref2;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *horizontalSpacer_9;
    QWidget *widget_5;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QCheckBox *checkPace;
    QSpacerItem *horizontalSpacer_4;

    void setupUi(QDialog *ConfigVX3p)
    {
        if (ConfigVX3p->objectName().isEmpty())
            ConfigVX3p->setObjectName(QString::fromUtf8("ConfigVX3p"));
        ConfigVX3p->resize(387, 469);
        ConfigVX3p->setSizeGripEnabled(true);
        ConfigVX3p->setModal(true);
        gridLayout = new QGridLayout(ConfigVX3p);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        widget = new QWidget(ConfigVX3p);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_7 = new QGridLayout(widget);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer, 1, 0, 1, 2);

        horizontalSpacer_3 = new QSpacerItem(20, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer_3, 1, 4, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer_5, 1, 6, 1, 1);

        widget_6 = new QWidget(widget);
        widget_6->setObjectName(QString::fromUtf8("widget_6"));
        horizontalLayout = new QHBoxLayout(widget_6);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_10);

        butRead = new QPushButton(widget_6);
        butRead->setObjectName(QString::fromUtf8("butRead"));

        horizontalLayout->addWidget(butRead);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_11);


        gridLayout_7->addWidget(widget_6, 0, 0, 1, 7);

        butSave = new QPushButton(widget);
        butSave->setObjectName(QString::fromUtf8("butSave"));

        gridLayout_7->addWidget(butSave, 1, 5, 1, 1);

        butCancel = new QPushButton(widget);
        butCancel->setObjectName(QString::fromUtf8("butCancel"));

        gridLayout_7->addWidget(butCancel, 1, 3, 1, 1);


        gridLayout->addWidget(widget, 7, 0, 1, 4);

        widget_2 = new QWidget(ConfigVX3p);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        gridLayout_2 = new QGridLayout(widget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lblName = new QLabel(widget_2);
        lblName->setObjectName(QString::fromUtf8("lblName"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        lblName->setFont(font);
        lblName->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(lblName, 0, 0, 1, 1);

        editName = new QLineEdit(widget_2);
        editName->setObjectName(QString::fromUtf8("editName"));
        editName->setMaxLength(18);

        gridLayout_2->addWidget(editName, 0, 1, 1, 1);

        lblId = new QLabel(widget_2);
        lblId->setObjectName(QString::fromUtf8("lblId"));
        lblId->setFont(font);
        lblId->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(lblId, 1, 0, 1, 1);

        editId = new QLineEdit(widget_2);
        editId->setObjectName(QString::fromUtf8("editId"));
        editId->setMaxLength(18);

        gridLayout_2->addWidget(editId, 1, 1, 1, 1);


        gridLayout->addWidget(widget_2, 0, 1, 2, 1);

        widget_3 = new QWidget(ConfigVX3p);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        gridLayout_3 = new QGridLayout(widget_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(-1, 4, -1, 4);
        groupSampleRate = new QGroupBox(widget_3);
        groupSampleRate->setObjectName(QString::fromUtf8("groupSampleRate"));
        groupSampleRate->setAlignment(Qt::AlignCenter);
        verticalLayout = new QVBoxLayout(groupSampleRate);
        verticalLayout->setSpacing(1);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(-1, 4, -1, 1);
        radioSampleRate1 = new QRadioButton(groupSampleRate);
        radioSampleRate1->setObjectName(QString::fromUtf8("radioSampleRate1"));

        verticalLayout->addWidget(radioSampleRate1);

        radioSampleRate2 = new QRadioButton(groupSampleRate);
        radioSampleRate2->setObjectName(QString::fromUtf8("radioSampleRate2"));

        verticalLayout->addWidget(radioSampleRate2);

        radioSampleRate3 = new QRadioButton(groupSampleRate);
        radioSampleRate3->setObjectName(QString::fromUtf8("radioSampleRate3"));

        verticalLayout->addWidget(radioSampleRate3);

        radioSampleRate4 = new QRadioButton(groupSampleRate);
        radioSampleRate4->setObjectName(QString::fromUtf8("radioSampleRate4"));

        verticalLayout->addWidget(radioSampleRate4);


        gridLayout_3->addWidget(groupSampleRate, 0, 1, 2, 1);

        groupDuration = new QGroupBox(widget_3);
        groupDuration->setObjectName(QString::fromUtf8("groupDuration"));
        groupDuration->setAlignment(Qt::AlignCenter);
        verticalLayout_2 = new QVBoxLayout(groupDuration);
        verticalLayout_2->setSpacing(1);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, 4, -1, 1);
        radioDuration96 = new QRadioButton(groupDuration);
        radioDuration96->setObjectName(QString::fromUtf8("radioDuration96"));

        verticalLayout_2->addWidget(radioDuration96);

        radioDuration72 = new QRadioButton(groupDuration);
        radioDuration72->setObjectName(QString::fromUtf8("radioDuration72"));

        verticalLayout_2->addWidget(radioDuration72);

        radioDuration48 = new QRadioButton(groupDuration);
        radioDuration48->setObjectName(QString::fromUtf8("radioDuration48"));

        verticalLayout_2->addWidget(radioDuration48);

        radioDuration24 = new QRadioButton(groupDuration);
        radioDuration24->setObjectName(QString::fromUtf8("radioDuration24"));

        verticalLayout_2->addWidget(radioDuration24);


        gridLayout_3->addWidget(groupDuration, 0, 2, 2, 1);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_12, 0, 0, 1, 1);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_13, 0, 3, 1, 1);


        gridLayout->addWidget(widget_3, 2, 1, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_7, 0, 0, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_6, 0, 2, 1, 1);

        widget_4 = new QWidget(ConfigVX3p);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        gridLayout_6 = new QGridLayout(widget_4);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setVerticalSpacing(6);
        gridLayout_6->setContentsMargins(-1, 0, -1, 0);
        groupBox_2 = new QGroupBox(widget_4);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(180, 16777215));
        groupBox_2->setAlignment(Qt::AlignCenter);
        gridLayout_4 = new QGridLayout(groupBox_2);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(-1, 4, -1, -1);
        radioDatePref1 = new QRadioButton(groupBox_2);
        radioDatePref1->setObjectName(QString::fromUtf8("radioDatePref1"));

        gridLayout_4->addWidget(radioDatePref1, 0, 1, 1, 1);

        radioDatePref2 = new QRadioButton(groupBox_2);
        radioDatePref2->setObjectName(QString::fromUtf8("radioDatePref2"));

        gridLayout_4->addWidget(radioDatePref2, 1, 1, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_8, 0, 0, 1, 1);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_9, 0, 2, 1, 1);


        gridLayout_6->addWidget(groupBox_2, 0, 1, 1, 1);


        gridLayout->addWidget(widget_4, 5, 1, 1, 1);

        widget_5 = new QWidget(ConfigVX3p);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        horizontalLayout_2 = new QHBoxLayout(widget_5);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 0, -1, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        checkPace = new QCheckBox(widget_5);
        checkPace->setObjectName(QString::fromUtf8("checkPace"));
        checkPace->setFont(font);

        horizontalLayout_2->addWidget(checkPace);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        gridLayout->addWidget(widget_5, 3, 1, 1, 1);

        QWidget::setTabOrder(editName, editId);
        QWidget::setTabOrder(editId, radioSampleRate1);
        QWidget::setTabOrder(radioSampleRate1, radioSampleRate2);
        QWidget::setTabOrder(radioSampleRate2, radioSampleRate3);
        QWidget::setTabOrder(radioSampleRate3, radioSampleRate4);
        QWidget::setTabOrder(radioSampleRate4, radioDuration96);
        QWidget::setTabOrder(radioDuration96, radioDuration72);
        QWidget::setTabOrder(radioDuration72, radioDuration48);
        QWidget::setTabOrder(radioDuration48, radioDuration24);
        QWidget::setTabOrder(radioDuration24, checkPace);
        QWidget::setTabOrder(checkPace, radioDatePref1);
        QWidget::setTabOrder(radioDatePref1, radioDatePref2);
        QWidget::setTabOrder(radioDatePref2, butRead);
        QWidget::setTabOrder(butRead, butCancel);
        QWidget::setTabOrder(butCancel, butSave);

        retranslateUi(ConfigVX3p);

        butSave->setDefault(true);


        QMetaObject::connectSlotsByName(ConfigVX3p);
    } // setupUi

    void retranslateUi(QDialog *ConfigVX3p)
    {
        ConfigVX3p->setWindowTitle(QCoreApplication::translate("ConfigVX3p", "Config VX3+", nullptr));
        butRead->setText(QCoreApplication::translate("ConfigVX3p", "Read SD Card", nullptr));
        butSave->setText(QCoreApplication::translate("ConfigVX3p", "Save to SD Card", nullptr));
        butCancel->setText(QCoreApplication::translate("ConfigVX3p", "Cancel", nullptr));
        lblName->setText(QCoreApplication::translate("ConfigVX3p", "Name", nullptr));
        lblId->setText(QCoreApplication::translate("ConfigVX3p", "ID", nullptr));
        groupSampleRate->setTitle(QCoreApplication::translate("ConfigVX3p", "Sample Rate", nullptr));
        radioSampleRate1->setText(QCoreApplication::translate("ConfigVX3p", "128", nullptr));
        radioSampleRate2->setText(QCoreApplication::translate("ConfigVX3p", "256", nullptr));
        radioSampleRate3->setText(QCoreApplication::translate("ConfigVX3p", "512", nullptr));
        radioSampleRate4->setText(QCoreApplication::translate("ConfigVX3p", "1024", nullptr));
        groupDuration->setTitle(QCoreApplication::translate("ConfigVX3p", "Duration", nullptr));
        radioDuration96->setText(QCoreApplication::translate("ConfigVX3p", "96", nullptr));
        radioDuration72->setText(QCoreApplication::translate("ConfigVX3p", "72", nullptr));
        radioDuration48->setText(QCoreApplication::translate("ConfigVX3p", "48", nullptr));
        radioDuration24->setText(QCoreApplication::translate("ConfigVX3p", "24", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("ConfigVX3p", "Date Preference", nullptr));
        radioDatePref1->setText(QCoreApplication::translate("ConfigVX3p", "MM/DD/YY", nullptr));
        radioDatePref2->setText(QCoreApplication::translate("ConfigVX3p", "DD/MM/YY", nullptr));
        checkPace->setText(QCoreApplication::translate("ConfigVX3p", "Pace Detection", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ConfigVX3p: public Ui_ConfigVX3p {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIGVX3P_H
