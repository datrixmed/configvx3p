#ifndef CONFIGVX3P_H
#define CONFIGVX3P_H

#include <QDialog>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QDir>

namespace Ui {
	class ConfigVX3p;
}

class ConfigVX3p : public QDialog
{
	Q_OBJECT

public:
	explicit ConfigVX3p(QWidget *parent = 0, bool withoutGui = 0 );
	~ConfigVX3p();

private:
	Ui::ConfigVX3p *ui;

protected:
	void setDurationConsistency(void);
	void setSampleRateConsistency(void);

protected:
	QTimer *timerLookForDrive;
	QString pathnameConfig;

private slots:
	void on_butSave_clicked();
	void on_butCancel_clicked();

	void on_radioSampleRate1_clicked();
	void on_radioSampleRate2_clicked();
	void on_radioSampleRate3_clicked();
	void on_radioSampleRate4_clicked();
	void on_radioDuration96_clicked();
	void on_radioDuration72_clicked();
	void on_radioDuration48_clicked();
	void on_radioDuration24_clicked();

	void readHolterCard(void);

	void on_butRead_clicked();

public:
	struct datrix_VX3p_config_File
	{
		unsigned short  firmware_rev;                   // OFFSET 0. WILL INCREMENT WHEN FILE FORMAT CHANGES
		unsigned char file_orig;                      // OFFSET 2. 0 = THIS FILE GEN BY RECORDER. 1 = GEN BY COMPUTER
		unsigned char recording_month;                // OFFSET 3. START TIME MONTH. ALWAYS SET BY RECORDER
		unsigned char recording_day;                  // OFFSET 4. START TIME DAY. ALWAYS SET BY RECORDER
		char          hw_rev;                 // OFFSET 5  NOT USED
		unsigned short  recording_year;                 // OFFSET 6. START TIME YEAR. ALWAYS SET BY RECORDER
		unsigned char date_display_pref;              // OFFSET 8. DATE DISPLAY PREFERENCE. 0 = MM/DD/YY. 1 = DD/MM/YY
		unsigned char time_display_pref;              // OFFSET 9. TIME DISPLAY PREFERENCE 0 = 12 HOUR. 1 = 24 HOUR FORMAT
		unsigned char start_hour;                             // OFFSET 10. START TIME HOUR
		unsigned char start_minute;                   // OFFSET 11. START TIME MINUTE
		unsigned char start_second;                   // OFFSET 12. START TIME SECOND
		unsigned char channels;                               // CHANNEL SETTING. 1 = 3 CHAN 5 LEAD. 2 = 3 CHAN 7 LD
		unsigned char desired_recording_time; // THIS NUMBER IS CHANGED BY THE RECORDER DEPENDING ON SAMPLE RATE
		unsigned char total_events;           // INC WHEN EVENT BUTTON IS PRESSED
		unsigned short  sample_rate;                    // SELECTED SAMPLE RATE. 128. 256. 512. 1024
		unsigned char file_read_status;               // 1 = FILE HAS BEEN READ BY PC. SET BY PC.
		unsigned char language_pref;          // 1 = ENGLISH
		char          pat_name[18];                   // PATIENTS FIRST NAME. When file read status is set to 1 the patient. NUL TERMINATED
		char          pat_id[18];                             // PATIENT ID NUMBER. NULL TERMINATED
		unsigned char recording_bits;         // DEFAULT 10; // 10 BITS ARE STORED IN 4 BYTES. 12 BITS TBD
		unsigned char rec_flags;                              // RECORDING FLAGS. 1 = PACE DETECTION ON
		unsigned char OEM_CODE;                       // FACTORY SET
		unsigned char error_flags;            // 0x80 = DISK ERROR.

		unsigned long recording_duration;         // ACTUAL SAMPLES RECORDED. UPDATE RATE ?
		unsigned long total_pace_detects;     // TOTAL PACE DETECTIONS WHEN PACE IS ENABLED
		unsigned long recorder_sensitivity;   // IN NANO VOLTS. USED FOR CALIBRATION. VARIES WITH BIT RESOLUTION. 0x2625 FOR 10 BIT. 0X989 FOR 12 BIT.
		unsigned long rec_serial_no;                  // RECORDER SERIAL NUMBER. DEPENDS ON OEM
		char txt_serial_no[12];                               // RECORDER SERIAL NUMBER. DEPENDS ON OEM. MAY NOT BE RELATED TO SERIAL NO ABOVE. STORED IN EPROM
		unsigned short  start_batV;             // STARTING BATT VOLTAGE
		unsigned short  end_batv;               // END BATTERY VOLTAGE. UNIT WILL AUTO SHUTDOWN WHEN BATTERY VOLTAGE IS TOO LOW.

	} datrixVX3pConfig;

};

#endif // CONFIGVX3P_H
