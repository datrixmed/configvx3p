
#include "configvx3p.h"
#include "ui_configvx3p.h"


/** {{{ ConfigVX3p::ConfigVX3p(QWidget *parent) : QDialog(parent), ui(new Ui::ConfigVX3p)
 */
ConfigVX3p::ConfigVX3p(QWidget *parent, bool withoutGui ) : QDialog(parent), ui(new Ui::ConfigVX3p)
{
	ui->setupUi(this);

	if ( withoutGui ) {
		hide();
	}

	memset( (void*) &datrixVX3pConfig, 0, sizeof(datrixVX3pConfig) );
	pathnameConfig = "";

	readHolterCard();
}
/* }}} */

/** {{{ ConfigVX3p::~ConfigVX3p()
 */
ConfigVX3p::~ConfigVX3p()
{
	close();

	if ( ui ) {
		delete ui;
	}
}
/* }}} */


/** {{{ void ConfigVX3p::on_butRead_clicked()
 */
void ConfigVX3p::on_butRead_clicked()
{
	readHolterCard();
}
/* }}} */


/** {{{ void ConfigVX3p::on_butSave_clicked()
 */
void ConfigVX3p::on_butSave_clicked()
{
	QFile vx3ConfigFile( pathnameConfig );

	if ( ! vx3ConfigFile.open(QIODevice::WriteOnly) ) {
		qDebug() << "Error opening VX3+ Config file";
		return;
	}

	if ( ! ui ) {
		return;
	}

	QDataStream vx3ConfigData(&vx3ConfigFile);

	if ( ui->radioSampleRate1->isChecked() ) { datrixVX3pConfig.sample_rate = 128; }
	if ( ui->radioSampleRate2->isChecked() ) { datrixVX3pConfig.sample_rate = 256; }
	if ( ui->radioSampleRate3->isChecked() ) { datrixVX3pConfig.sample_rate = 512; }
	if ( ui->radioSampleRate4->isChecked() ) { datrixVX3pConfig.sample_rate = 1024; }

	if ( ui->radioDuration24->isChecked() ) { datrixVX3pConfig.desired_recording_time = 24; }
	if ( ui->radioDuration48->isChecked() ) { datrixVX3pConfig.desired_recording_time = 48; }
	if ( ui->radioDuration72->isChecked() ) { datrixVX3pConfig.desired_recording_time = 72; }
	if ( ui->radioDuration96->isChecked() ) { datrixVX3pConfig.desired_recording_time = 96; }

	memcpy( datrixVX3pConfig.pat_name, ui->editName->text().toUtf8().constData(), sizeof(datrixVX3pConfig.pat_name) );
	memset( &(datrixVX3pConfig.pat_name[strlen(datrixVX3pConfig.pat_name)]), 0, sizeof(datrixVX3pConfig.pat_name) - strlen(datrixVX3pConfig.pat_name) );
	memcpy( datrixVX3pConfig.pat_id, ui->editId->text().toUtf8().constData(), sizeof(datrixVX3pConfig.pat_id) );
	memset( &(datrixVX3pConfig.pat_id[strlen(datrixVX3pConfig.pat_id)]), 0, sizeof(datrixVX3pConfig.pat_id) - strlen(datrixVX3pConfig.pat_id) );

	datrixVX3pConfig.date_display_pref = ui->radioDatePref2->isChecked();

	datrixVX3pConfig.rec_flags = ui->checkPace->isChecked();

	datrixVX3pConfig.time_display_pref = 1;		/* Lynda Cole wants this always to be the 24hr format */

	datrixVX3pConfig.file_read_status = 0;
	datrixVX3pConfig.file_orig = 1;

	vx3ConfigData.writeRawData( (char*) &datrixVX3pConfig, sizeof(datrixVX3pConfig) );


	close();
}
/* }}} */


/** {{{ void ConfigVX3p::on_butCancel_clicked()
 */
void ConfigVX3p::on_butCancel_clicked()
{
	close();
}
/* }}} */


/** {{{ void ConfigVX3p::on_radioSampleRate1_clicked()
 */
void ConfigVX3p::on_radioSampleRate1_clicked()
{
	setDurationConsistency();
}
/* }}} */


/** {{{ void ConfigVX3p::on_radioSampleRate2_clicked()
 */
void ConfigVX3p::on_radioSampleRate2_clicked()
{
	setDurationConsistency();
}
/* }}} */

/** {{{ void ConfigVX3p::on_radioSampleRate3_clicked()
 */
void ConfigVX3p::on_radioSampleRate3_clicked()
{
	setDurationConsistency();
}
/* }}} */

/** {{{ void ConfigVX3p::on_radioSampleRate4_clicked()
 */
void ConfigVX3p::on_radioSampleRate4_clicked()
{
	setDurationConsistency();
}
/* }}} */

/** {{{ void ConfigVX3p::on_radioDuration96_clicked()
 */
void ConfigVX3p::on_radioDuration96_clicked()
{
	setSampleRateConsistency();
}
/* }}} */

/** {{{ void ConfigVX3p::on_radioDuration72_clicked()
 */
void ConfigVX3p::on_radioDuration72_clicked()
{
	setSampleRateConsistency();
}
/* }}} */

/** {{{ void ConfigVX3p::on_radioDuration48_clicked()
 */
void ConfigVX3p::on_radioDuration48_clicked()
{
	setSampleRateConsistency();
}
/* }}} */

/** {{{ void ConfigVX3p::on_radioDuration24_clicked()
 */
void ConfigVX3p::on_radioDuration24_clicked()
{
	setSampleRateConsistency();
}
/* }}} */


/** {{{ void ConfigVX3p::setSampleRateConsistency()
 */
void ConfigVX3p::setSampleRateConsistency()
{
	if ( ! ui ) {
		return;
	}

	if ( ui->radioDuration24->isChecked() ) {
		ui->radioSampleRate1->setEnabled(true);
		ui->radioSampleRate2->setEnabled(true);
		ui->radioSampleRate3->setEnabled(true);
		ui->radioSampleRate4->setEnabled(true);
	}
	if ( ui->radioDuration48->isChecked() ) {
		ui->radioSampleRate1->setEnabled(true);
		ui->radioSampleRate2->setEnabled(true);
		ui->radioSampleRate3->setEnabled(true);
		ui->radioSampleRate4->setEnabled(false);
	}
	if ( ui->radioDuration72->isChecked() ) {
		ui->radioSampleRate1->setEnabled(true);
		ui->radioSampleRate2->setEnabled(true);
		ui->radioSampleRate3->setEnabled(false);
		ui->radioSampleRate4->setEnabled(false);
	}
	if ( ui->radioDuration96->isChecked() ) {
		ui->radioSampleRate1->setEnabled(true);
		ui->radioSampleRate2->setEnabled(false);
		ui->radioSampleRate3->setEnabled(false);
		ui->radioSampleRate4->setEnabled(false);
	}
}
/* }}} */


/** {{{ void ConfigVX3p::setDurationConsistency()
 */
void ConfigVX3p::setDurationConsistency()
{
	if ( ! ui ) {
		return;
	}

	if ( ui->radioSampleRate1->isChecked() ) {
		ui->radioDuration24->setEnabled(true);
		ui->radioDuration48->setEnabled(true);
		ui->radioDuration72->setEnabled(true);
		ui->radioDuration96->setEnabled(true);
	}
	if ( ui->radioSampleRate2->isChecked() ) {
		ui->radioDuration24->setEnabled(true);
		ui->radioDuration48->setEnabled(true);
		ui->radioDuration72->setEnabled(true);
		ui->radioDuration96->setEnabled(false);
	}
	if ( ui->radioSampleRate3->isChecked() ) {
		ui->radioDuration24->setEnabled(true);
		ui->radioDuration48->setEnabled(true);
		ui->radioDuration72->setEnabled(false);
		ui->radioDuration96->setEnabled(false);
	}
	if ( ui->radioSampleRate4->isChecked() ) {
		ui->radioDuration24->setEnabled(true);
		ui->radioDuration48->setEnabled(false);
		ui->radioDuration72->setEnabled(false);
		ui->radioDuration96->setEnabled(false);
	}
}
/* }}} */


/** {{{ void readHolterCard(void)
 */
void ConfigVX3p::readHolterCard(void)
{
	QFileInfoList allDrivesList = QDir::drives();

	// Try every known drive letter...
	foreach ( QFileInfo driveInfo, allDrivesList ) {

		// qDebug() << "Drive: " << QDir::cleanPath( driveInfo.canonicalPath() + QDir::separator() );

		if ( QFile::exists( QDir::cleanPath( driveInfo.canonicalPath() + QDir::separator() + "config.dat" ) ) ) {

			pathnameConfig = QString( QDir::cleanPath(driveInfo.canonicalPath() + QDir::separator() + "CONFIG.DAT") );

			QFile vx3ConfigFile( pathnameConfig );

			if ( ! vx3ConfigFile.open(QIODevice::ReadWrite) ) {
				qDebug() << "Error opening VX3+ Config file";
				return;
			}

			QDataStream vx3ConfigData(&vx3ConfigFile);

			vx3ConfigData.readRawData( (char*) &datrixVX3pConfig, sizeof(datrixVX3pConfig) );

#ifdef DEBUG
			qDebug() << "name = " << QString(datrixVX3pConfig.pat_name).left(sizeof(datrixVX3pConfig.pat_name));
			qDebug() << "id = " << QString(datrixVX3pConfig.pat_id).left(sizeof(datrixVX3pConfig.pat_id));
			qDebug() << "sample_rate = " << datrixVX3pConfig.sample_rate;
			qDebug() << "desired_recording_time = " << datrixVX3pConfig.desired_recording_time;
			qDebug() << "date pref = " << datrixVX3pConfig.date_display_pref;
			qDebug() << "pace detection = " << datrixVX3pConfig.rec_flags;
#endif

			ui->editName->setText( QString(datrixVX3pConfig.pat_name).left(sizeof(datrixVX3pConfig.pat_name)) );
			ui->editId->setText( QString(datrixVX3pConfig.pat_id).left(sizeof(datrixVX3pConfig.pat_id)) );

			ui->radioDatePref1->setChecked( datrixVX3pConfig.date_display_pref == 0 );
			ui->radioDatePref2->setChecked( datrixVX3pConfig.date_display_pref == 1 );

			ui->checkPace->setChecked( datrixVX3pConfig.rec_flags );

			ui->radioSampleRate1->setChecked(false);
			ui->radioSampleRate2->setChecked(false);
			ui->radioSampleRate3->setChecked(false);
			ui->radioSampleRate4->setChecked(false);

			ui->radioDuration24->setChecked(false);
			ui->radioDuration48->setChecked(false);
			ui->radioDuration72->setChecked(false);
			ui->radioDuration96->setChecked(false);

			switch ( datrixVX3pConfig.sample_rate ) {
				case 128: ui->radioSampleRate1->setChecked(true); break;
				case 256: ui->radioSampleRate2->setChecked(true); break;
				case 512: ui->radioSampleRate3->setChecked(true); break;
				case 1024: ui->radioSampleRate4->setChecked(true); break;
			}

			switch ( datrixVX3pConfig.desired_recording_time ) {
				case 24: ui->radioDuration24->setChecked(true); break;
				case 48: ui->radioDuration48->setChecked(true); break;
				case 72: ui->radioDuration72->setChecked(true); break;
				case 96: ui->radioDuration96->setChecked(true); break;
			}

			setSampleRateConsistency();
			setDurationConsistency();

			ui->editName->selectAll();

			return;
		}
	}

	qDebug() << "Error finding VX3+ Config file on SD Card";
	return;
}
/* }}} */






#ifdef Q_OS_WIN32

#include <qt_windows.h>

/** {{{ QString getVolumeLabel(QString & tdrive)
 * Function to retrieve volume names
 */
QString getVolumeLabel(QString & tdrive)
{
	WCHAR szVolumeName[256] ;
	WCHAR szFileSystemName[256];
	DWORD dwSerialNumber = 0;
	DWORD dwMaxFileNameLength=256;
	DWORD dwFileSystemFlags=0;
	bool ret = GetVolumeInformation( (WCHAR *) tdrive.utf16(),szVolumeName,256,&dwSerialNumber,&dwMaxFileNameLength,&dwFileSystemFlags,szFileSystemName,256);
	if(!ret)return QString("");
	QString vName=QString::fromUtf16 ( (const ushort *) szVolumeName) ;
	vName.trimmed();
	return vName;
}
/* }}} */

#else

/** {{{ QString getVolumeLabel(QString & tdrive)
 * Function to retrieve volume names
 */
QString getVolumeLabel(QString & tdrive)
{
	 return tdrive;
}
/* }}} */

#endif

