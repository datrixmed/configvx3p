#!/bin/bash

REV=$(svn info | grep "Last Changed Rev" | awk '{ print $4 }')

if [ "$REV" == "" ]; then
	REV=$(git log | head -1 | awk '{ print $2 }' | cut -f6 )
	REV="${REV: -6}"
#	REV=$((16#$REV))
fi

cp -f installer/ConfigVx3-linux.iss /tmp/ConfigVx3-linux.iss
sed --in-place "s/define MyVersion.*/define MyVersion\t\t\"$REV\"/" /tmp/ConfigVx3-linux.iss
diff installer/ConfigVx3-linux.iss /tmp/ConfigVx3-linux.iss
if [ $? -ne 0 ]
then
	mv -f /tmp/ConfigVx3-linux.iss installer/ConfigVx3-linux.iss
	echo Updated ConfigVx3-linux.iss to Rev: $REV
fi

cp -f version.h /tmp/version.h
sed --in-place "s/glbRevision.*/glbRevision = \"$REV\";/" /tmp/version.h
sed --in-place "s/glbDateLastModified.*/glbDateLastModified = \"$(date +'%F %H:%M')\";/" /tmp/version.h

diff /tmp/version.h version.h
if [ $? -ne 0 ]
then
	mv -f /tmp/version.h version.h
	echo Updated version.h to Rev: $REV
fi
