
QT       += \
			gui \
			widgets \

TEMPLATE = app

TARGET = ConfigVx3

greaterThan(QT_MAJOR_VERSION, 4) {
	QT += widgets
}

QMAKE_EXTRA_TARGETS += version

CONFIG += qt \
        thread \
        c++11

QT += network
QT -= testlib

DESTDIR = out
MOC_DIR = tmp/moc
UI_DIR = tmp/ui

INCLUDEPATH += tmp/ui


win32: LIBS += -static-libgcc -static-libstdc++

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.


CONFIG(debug, debug|release) {
        TARGET = ConfigVx3.debug
        OBJECTS_DIR = tmp/debug
        RCC_DIR = tmp/debug/rcc
}
CONFIG(release, debug|release) {
        TARGET = ConfigVx3
        OBJECTS_DIR = tmp/release
        RCC_DIR = tmp/release/rcc
        QT -= testlib
}

unix {
}

# Define how to create version.h
win32 {
	version.target = version.h
	equals(QMAKE_CXX, g++):version.commands = versionit.bat
	!equals(QMAKE_CXX, g++):version.commands = ./subwcrev.sh "version.tmpl" "version.h"
	version.depends = FORCE

	installer.path = installer
	installer.commands = docker run --rm -i -v "$$PWD:/work" amake/innosetup installer/*-linux.iss
	INSTALLS += installer
}


SOURCES += \
		   main.cpp \
		   configvx3p.cpp \

HEADERS  += configvx3p.h \

FORMS    += configvx3p.ui \



